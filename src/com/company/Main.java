/*

Charlie Murphy
Book of Bacon
27/02/2018


 */


package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import static java.lang.Integer.parseInt;
import static java.lang.System.nanoTime;
import static java.lang.System.out;

class Main {

    public static void main(String[] args) {

        // data structures
        TreeSet<String> uniqueWordSet = new TreeSet<>();
        Map<Integer, String[]> sortedBook = new TreeMap<>();

        // variable used to steps the program's runt ime
        long startTime = nanoTime();

        // returns input data in a TreeMap
        sortInput(sortedBook);

        // Searches the TreeMap for code word
        findUniqueWords(uniqueWordSet, sortedBook, startTime);


    }

    // Iterates through TreeMap collecting unique words via storing new words in a TreeSet to prevent duplication
    private static void findUniqueWords(TreeSet<String> uniqueWordSet, Map<Integer, String[]> sortedBook, long startTime) {

        String uniqueWord;
        int position;
        int steps;
        boolean stopSearch;

        // Run through every entry so every unique word is found
        for (Map.Entry<Integer, String[]> entry : sortedBook.entrySet()) {

            // make sure to search every value
            // iterator starts at two due to null values
            for (int x = 2; x < entry.getValue().length; x++) {

                // prevents duplicate word searches
                if (!uniqueWordSet.contains(entry.getValue()[x])) {

                    // unique word is held temporarily
                    uniqueWord = entry.getValue()[x];

                    // add unique word to set to prevent duplicate word searches
                    uniqueWordSet.add(entry.getValue()[x]);

                    // keep position in temp variable so code does not steps words from beginning
                    position = entry.getKey();

                    // reset steps for each iteration
                    steps = 0;
                    stopSearch = false;

                    // searches for the next appearance of the unique word in the collection
                    wordSearch(sortedBook, steps, stopSearch, uniqueWord, position, startTime);


                    // skips word if it has already been searched
                } else {
                    break;
                }

            }


        }
    }

    // Uses the current unique word to check whether it matches the condition to be the code word
    private static void wordSearch(Map<Integer, String[]> sortedBook, int steps, boolean stopSearch, String uniqueWord, int position, long startTime) {
        String codeWord;

        while (!stopSearch) {

            // iterates from position where the unique word was first found (to prevent searching from collection's start)
            for (int t = position; t < sortedBook.size(); t++) {

                // breaks iteration for unique word because it has been found
                if (stopSearch) {
                    break;
                }

                // i starts at 2 because of 'null' values in [0] [1] indexes
                for (int i = 2; i < sortedBook.get(t).length; i++) {

                    // doesn't match the condition, skip this word
                    if (steps > 666) {
                        stopSearch = true;
                    }

                    // if it is found again, record the steps
                    if (sortedBook.get(t)[i].matches(uniqueWord) && (t != position)) {

                        // breaks iteration
                        stopSearch = true;

                        // stops searching with unique words if the precise condition is matched
                        if (steps == 666) {

                            // store codeword to find message
                            codeWord = sortedBook.get(t)[i];
                            out.println("Code word: " + "\n" + codeWord + "\n" + "Secret message:");

                            // uses code word to find message
                            findMessage(sortedBook, codeWord);

                            // prints program run duration
                            long endTime = nanoTime();
                            out.print(
                                    "\n"
                                    + "Time: "
                                    + TimeUnit.SECONDS.convert(endTime - startTime, TimeUnit.NANOSECONDS)
                                    + " seconds");

                        }

                        break;


                        // if the word doesn't match, then increase the number of steps
                    } else if (!sortedBook.get(t)[i].matches(uniqueWord)) {
                        steps++;
                    }

                }

            }
        }
    }


    // iterates through collection using codeword to find secret message
    private static void findMessage(Map<Integer, String[]> sortedBook, String codeWord) {

        // iterate through the data now looking for the codeword
        for (Map.Entry<Integer, String[]> entry2 : sortedBook.entrySet()) {

            for (int d = 2; d < entry2.getValue().length; d++) {

                // prints the value of the following word after the codeword
                if (entry2.getValue()[d].matches(codeWord)) {
                    out.print(entry2.getValue()[d + 1] + " ");
                }
            }
        }
    }

    // reads and stores the input text file in a TreeMap collection
    protected static void sortInput(Map<Integer, String[]> sortedBook) {

        // fastest means Java provides to parse a large file
        FileReader fr;
        BufferedReader br;

        try {

            fr = new FileReader("resources/bacon.txt");
            br = new BufferedReader(fr);


            String currentLine;
            String[] currentWords;
            int lineNo = 0;

            while ((currentLine = br.readLine()) != null) {

                // convert the book line into separate words
                currentWords = currentLine.split(" ");

                // store the number in a temp variable to put as key in Map
                try {
                    lineNo = parseInt(currentWords[0]);

                } catch (NumberFormatException ignored) {

                }

                // Remove # and line number as they are now redundant
                currentWords[0] = null;
                currentWords[1] = null;

                // Insert line number and associated line into TreeMap for natural sort
                sortedBook.put(lineNo, currentWords);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
