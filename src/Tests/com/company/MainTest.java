package com.company;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import static com.company.Main.sortInput;
import static java.lang.System.nanoTime;
import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MainTest {

    // test for the sortInput method
    @Test
    void sortInputShouldReturnFilledMap() {

        // Arrange - test variable
        Map<Integer, String[]> testMap = new TreeMap<>();

        // Act - uses method that requires testing
        sortInput(testMap);

        // Assert - checks whether the collection added anything
        assertEquals(true, testMap.values().size() > 1);

        // checks whether map uses integers as keys
        assertEquals(true, testMap.containsKey(testMap.size()));

        // checks whether map's stored arrays hold strings
        assertEquals(true, !testMap.values().isEmpty());
    }

    // test for the findUniqueWords method
    @Test
    void findUniqueWordsShouldNotUseDuplicates() {

        // Arrange - test variables
        Map<Integer, String[]> testMap = new TreeMap<>();
        TreeSet<String> testSet = new TreeSet<>();
        String uniqueWord = null;
        int position = 0;

        // Act - use method that requires testing
        sortInput(testMap);

        // repeat the logic of the main method
        for (Map.Entry<Integer, String[]> entry : testMap.entrySet()) {


            for (int x = 2; x < entry.getValue().length; x++) {

                if (!testSet.contains(entry.getValue()[x])) {

                    uniqueWord = entry.getValue()[x];

                    testSet.add(entry.getValue()[x]);

                    position = entry.getKey();
                }
            }


            // Assert that the initial values have changed
            assertEquals(true, uniqueWord != null);
            assertEquals(true, position > 0);

        }
    }


    // test for the findMessage method
    @Test
    void findMessageShouldRetrieveAFollowingWord() {

        // Arrange - test variable
        Map<Integer, String[]> testMap = new TreeMap<>();
        String uniqueWord = null;
        String message = null;

        // Act - use method that requires testing
        sortInput(testMap);

        // repeat the logic of the main method
        for (Map.Entry<Integer, String[]> entry : testMap.entrySet()) {


            for (int x = 2; x < entry.getValue().length - 1; x++) {

                // pretend the uniqueWord is the codeword
                uniqueWord = entry.getValue()[x];

                // store the following word
                message = entry.getValue()[x + 1];

            }


            // Assert that the initial values have changed
            assertEquals(true, uniqueWord != null);
            assertEquals(true, message != null);

        }
    }

}